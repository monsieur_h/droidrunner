groundThread    =   MOAIThread.new()
groundThread:run( function()
    while true do
        for i, item in pairs( GROUND_POOL ) do
            -- Adapt speed of platforms to match the scroll
            item.body:setLinearVelocity( -chickProp.speed, 0 )

            --Remove platforms out of the screen
            x, y    =   item.body:getPosition()
            x   =   x + item.lenght * 32
            if x < -Stage.w/2 then
                table.remove( GROUND_POOL, i )
                destroyPlatform( item )
            end
        end
        
        --Decides if we should create a new platform
        if AUTO_CREATE_GROUND then
            local lastPlatform    =   GROUND_POOL[#GROUND_POOL]
            if lastPlatform then
                x, y    =   lastPlatform.body:getPosition()
                x   =   x + lastPlatform.lenght * 32
                if x < Stage.w then --Should be Stage.w/2, but since the camera unzooms, better be ahead of this
                    createNextGroundPlatform( x, y+lastPlatform.height*32, true, true )
                end
            end
        end
        coroutine.yield()
    end
end)

--Platform thread : manages the speed of platforms and removes them when out of the screen
platformThread  =   MOAIThread.new()
platformThread:run( function()
    while true do
        for i, item in pairs(PLATFORM_POOL) do
            -- Adapt speed of platforms to match the scroll
            item.body:setLinearVelocity( -chickProp.speed, 0 )

            --Remove platforms out of the screen
            x, y    =   item.body:getPosition()
            x   =   x + item.lenght * 32
            if x < -Stage.w/2 then
                table.remove( PLATFORM_POOL, i )
                destroyPlatform( item )
            end
        end

        --Decides if we should create a new platform
        if AUTO_CREATE_PLATFORMS then
            local lastPlatform    =   PLATFORM_POOL[#PLATFORM_POOL]
            if lastPlatform then
                x, y    =   lastPlatform.body:getPosition()
                x   =   x + lastPlatform.lenght * 32
                if x < Stage.w/2 then
                    createNextPlatform( x, y, true, true )
                end
            end
        end
        coroutine.yield()
    end
end)


--Animation and sound thread
MOAIThread.new():run(function()
  currentTime =   MOAISim.getDeviceTime()
  while true do
    --Running behavior
    if chickProp.animationState.current.name == 'run' then
        if chickProp.contactCount < 1 then
            chickProp.animationState:setAnimation( 'jump' )
        end
        local tmp   =   chickProp.speed/chickProp.maxSpeed * 2
        local dt    =   MOAISim.getStep() * tmp
        chickProp.animationState:update( dt )
    else
        chickProp.animationState:update( MOAISim.getStep() )
    end

    --Jumping behavior
    if chickProp.animationState.current.name == 'jump' then
        chickProp.animationState:update( MOAISim.getStep() )
        if chickProp.animationState.currentTime > chickProp.animationState.current.duration then
            chickProp.animationState:setAnimation("fall", true)
        end
    end

    --Bumping behavior
    if chickProp.animationState.current.name == 'bump' then
        chickProp.animationState:update( MOAISim.getStep() )
        if chickProp.animationState.currentTime > chickProp.animationState.current.duration then
            chickProp.animationState:setAnimation("run", true)
        end
    end

    --Falling behavior
    if chickProp.animationState.current.name == 'fall' then
        chickProp.animationState:update( MOAISim.getStep() )
        if chickProp.contactCount > 0 then
            chickProp.animationState:setAnimation("land", false)
        end
    end
    
    --Landing behavior
    if chickProp.animationState.current.name == 'land' then
        chickProp.animationState:update( MOAISim.getStep() )
        if chickProp.animationState.currentTime > chickProp.animationState.current.duration then
            chickProp.animationState:setAnimation("run", true)
        end
    end
    
    --Roaring behavior
    if chickProp.animationState.current.name == "roar" then 
        if not chickProp.sounds.playing and AUDIO then
            chickProp.sounds.playing    =   true
            chickProp.sounds.roar:play()
        end

        if chickProp.animationState.currentTime > chickProp.animationState.current.duration then
            chickProp.animationState:setAnimation("run", true)
            chickProp.sounds.playing    =   false
        end
    end


    chickProp.animationState:apply(chickProp.skeleton)
    chickProp.skeleton:updateWorldTransform()
    
    --TODO: EPIC HACK CORRECT THIS ASAP THX!
    local x, y  =   chickProp.body:getPosition() 
    chickProp.skeleton.prop:setLoc( x, y+15 )

    local   previousTime    =   currentTime
    currentTime =   MOAISim.getDeviceTime()
    DELTATIME   =   currentTime - previousTime
    chickProp:accelerate( DELTATIME )
    
    coroutine.yield()
  end
end)
