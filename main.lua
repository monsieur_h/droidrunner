DEBUG       =   true
--AUTO_CREATE_PLATFORMS  =   true
AUTO_CREATE_GROUND     =   true
GAMESTATE   =   'splash'
AUDIO       =   true
require 'utils'
require 'builder'
require 'graphics'
require 'hud'
require 'splash'
require 'physics'
require 'player'
require 'thread'

function gameStateHandler( pState )
    log('gameStateHandler : '..pState..' from '..GAMESTATE)
    if pState == 'gameover' then
        log('Game Over')
        chickProp.stoped    =   true
        scoreTextbox:setVisible( false )
        gameOverTextBox:setVisible( true )
    elseif pState == 'play' then
        clearColor  =   {
            171/255,
            049/255,
            074/255,
            255/255
        }
        updateClearColor( clearColor )
        MOAIRenderMgr.setRenderTable( layerTable )
        gameOverTextBox:setVisible( false )
        scoreTextbox:setVisible( true )
        scoreTextbox:setString( 'MY SCORE GOES HERE' )
        restartGame()

    elseif pState == 'splashscreen' then
        scoreTextbox:setVisible( false )
        gameOverTextBox:setVisible( false )
        if titleTextBox then
            titleTextBox:setVisible( true )
        end
        clearColor  =   {
            000/255,
            000/255,
            000/255,
            255/255
        }
        updateClearColor( clearColor )
        MOAIRenderMgr.setRenderTable( splashLayerTable )
    end
    GAMESTATE   =   pState
end

function restartGame()
    log('Restarting game...')
    camera:setMode( 'closeup' )
    chickProp.body:setTransform(0, 0, 0)
    chickProp.animationState:setAnimation( 'roar' , false )
    chickProp.score =   0
    chickProp.runDuration   =   0
    
    --Deleting platforms...
    for i=#PLATFORM_POOL, 1, -1 do
        destroyPlatform( PLATFORM_POOL[i] )
    end
    PLATFORM_POOL   =   {}

    --Deleting obstacles...
    for i=#OBSTACLE_POOL, 1, -1 do
        destroyObstacle( OBSTACLE_POOL[i] )
    end
    OBSTACLE_POOL   =   {}

    for i=#GROUND_POOL, 1, -1 do
        destroyGround( GROUND_POOL[i] )
    end
    GROUND_POOL     =   {}

    --Generating a platform under the player
    makePlatform( -128, -64, 50 )
    createNextGroundPlatform( -128, -128 )

    --Let it move now !
    chickProp.stoped    =   true
    chickProp.speed     =   100
    chickProp.contactCount  =   0
    chickProp.body:applyLinearImpulse( 1, 1 )
end

function bottomScreenSensorHandler( phase, fix_a, fix_b, arbiter)
    log('bottom sensor:'..fix_b:getBody().tag)
    if fix_b:getBody().tag == 'obstacle' then
        destroyObstacleByBody( fix_b:getBody() )
    elseif fix_b:getBody().tag == 'player' then
        print( 'Player hit' )
        if GAMESTATE == 'play' then 
            gameStateHandler( 'gameover' )
        end
    end
end
bottomScreenSensor.fixture:setCollisionHandler( bottomScreenSensorHandler )

gameStateHandler( 'splashscreen' )
