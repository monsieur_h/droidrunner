--Array holding the platforms and obstacles 
PLATFORM_POOL   =   {} 
OBSTACLE_POOL   =   {} 
GROUND_POOL     =   {}
--List of platforms and their indexes in the spritesheet
PLATFORM_TYPE   =   {
    largeBeam   =   {first=0x305, mid=0x306, last=0x307, single=0x304, height=1},
    smallBeam   =   {first=0x2d6, mid=0x2d6, last=0x2d6, single=0x2d6, height=0.5},
    smallBeamAlt=   {first=0x2b9, mid=0x2b9, last=0x2b9, single=0x2b9, height=0.5},
    largeBeam   =   {first=0x305, mid=0x306, last=0x307, single=0x304, height=1},
    list        =   {'largeBeam', 'smallBeam', 'smallBeamAlt', 'underBeam'}

}

PILLAR_TYPE     =   {
    standard    =   {bot=0x245, mid=0x20b, top=0x20b}
}

--List of possibles grounds
GROUND_TYPE     =   {
    full        =   {
                        topfirst=0x1,
                        topmid=0x1c5,
                        toplast=0x2,
                        single=0x1c5,
                        first=0x1c5,
                        last=0x1c5,
                        mid=0x1c5
                    },

    slab        =   {
                        first=0x291,
                        mid=0x1c5,
                        last=0x293,
                        single=0x304,
                        top=0x304,
                        topfirst=0x274,
                        toplast=0x276,
                        topmid=0x275
                    },

    list        =   {'full', 'slab'}

}

--List of decoration items avalaibles and their construction info
DECORATION_ARRAY    =   {
    chair           =   {
                            prop={
                                piv={x=16, y=16}
                            },
                            body={
                                mass=10.5
                            },
                            fixture={
                                shape='rect',
                                size={xMin=-12,yMin=-16,xMax=12,yMax=16},
                                restitution=0.01,
                                friction=1.0,
                                density=0.05
                            },
                            map={
                                {1, 0x43c}
                            }
                        },
    chairSide       =   {
                            prop={
                                piv={x=16, y=16}
                            },
                            body={
                                mass=10.5
                            },
                            fixture={
                                shape='rect',
                                size={xMin=-12,yMin=-16,xMax=12,yMax=16},
                                restitution=0.01,
                                friction=1.0,
                                density=0.05
                            },
                            map={
                                {1, 0x43d}
                            }
                        },
    deskChair       =   {
                            prop={
                                piv={x=16, y=16}
                            },
                            body={
                                mass=10.5
                            },
                            fixture={
                                shape='rect',
                                size={xMin=-12,yMin=-16,xMax=12,yMax=16},
                                restitution=0.01,
                                friction=1.0,
                                density=0.05
                            },
                            map={
                                {1, 0x43b}
                            }
                        },
    deskChairSide   =   {
                            prop={
                                piv={x=16, y=16}
                            },
                            body={
                                mass=10.5
                            },
                            fixture={
                                shape='rect',
                                size={xMin=-12,yMin=-16,xMax=12,yMax=16},
                                restitution=0.01,
                                friction=1.0,
                                density=0.05
                            },
                            map={
                                {1, 0x43a}
                            }
                        },
    desktopSide     =   {
                            prop={
                                piv={x=16, y=16}
                            },
                            body={
                                mass=10.5
                            },
                            fixture={
                                shape='rect',
                                size={xMin=-12,yMin=-16,xMax=12,yMax=16},
                                restitution=0.01,
                                friction=1.0,
                                density=0.05
                            },
                            map={
                                {1, 0x421}
                            }
                        },
    desktop         =   {
                            prop={
                                piv={x=16, y=16}
                            },
                            body={
                                mass=10.5
                            },
                            fixture={
                                shape='rect',
                                size={xMin=-12,yMin=-16,xMax=12,yMax=16},
                                restitution=0.01,
                                friction=1.0,
                                density=0.05
                            },
                            map={
                                {1, 0x422}
                            }
                        },
    longTable       =   {
                            prop={
                                piv={x=32, y=16}
                            },
                            body={
                                mass=10
                            },
                            fixture={
                                shape='rect',
                                size={xMin=-32, yMin=-16, xMax=32, yMax=9},
                                restitution=0.01,
                                friction=0.9,
                                density=0.15
                            },
                            map={
                                {1, 0x41f, 0x420}
                            },
                        },
    pillarMid       =   {
                            prop={
                                piv={x=16, y=32}
                            },
                            body={
                                mass=10
                            },
                            fixture={
                                shape='rect',
                                size={xMin=-12, yMin=-32, xMax=12, yMax=0},
                                restitution=0.1,
                                friction=1.0,
                                density=0.05
                            },
                            map={
                                {1, 0x20b}
                            },
                        },
    pillarBot       =   {
                            prop={
                                piv={x=16, y=32}
                            },
                            body={
                                mass=10
                            },
                            fixture={
                                shape='rect',
                                size={xMin=-12, yMin=-32, xMax=12, yMax=0},
                                restitution=0.1,
                                friction=1.0,
                                density=0.05
                            },
                            map={
                                {1, 0x245}
                            },
                        },
    pillarTop       =   {
                            prop={
                                piv={x=16, y=32}
                            },
                            body={
                                mass=10
                            },
                            fixture={
                                shape='rect',
                                size={xMin=-10, yMin=-32, xMax=10, yMax=32},
                                restitution=0.1,
                                friction=1.0,
                                density=0.05
                            },
                            map={
                                {2, 0x1ee},
                                {1, 0x20b}
                            },
                        },
    pillarHardTop   =   {
                            prop={
                                piv={x=48, y=16}
                            },
                            body={
                                mass=10
                            },
                            fixture={
                                shape='rect',
                                size={xMin=-48, yMin=-16, xMax=48, yMax=16},
                                restitution=0.1,
                                friction=1.0,
                                density=0.05
                            },
                            map={
                                {1, 0x229, 0x22a, 0x22b}
                            }
                        },
    pillarHardTopWide=  {
                            prop={
                                piv={x=80, y=16}
                            },
                            body={
                                mass=10
                            },
                            fixture={
                                shape='rect',
                                size={xMin=-80, yMin=-16, xMax=80, yMax=16},
                                restitution=0.1,
                                friction=1.0,
                                density=0.05
                            },
                            map={
                                {1, 0x229, 0x22a, 0x22a, 0x22a, 0x22b}
                            }
                        },
    list    =   {'chair', 'chairSide', 'desktop', 'desktopSide', 'deskChair', 'deskChairSide', 'longTable', 'pillarBot', 'pillarTop', 'pillarMid', 'pillarHardTop', 'pillarHardTopWide' }
}

--List of sets. A set is composed of several decorational items. Some collidable, some not
--in itemList, collidable enables it to have physics
--              bound, makes a distance joint with the previous element in the list (each from its pivot point)
DECORATION_SETS =   {
    meetingTable    =   {
        itemCount   =   5,
        lenght      =   80,  --in pixels
        itemList    =   {
            {name='chair', x=32, y=0, collidable=false},
            {name='chair', x=0, y=0, collidable=false},
            {name='longTable', x=16, y=0, collidable=true},
            {name='chairSide', x=49, y=0, collidable=true},
            {name='chairSide', x=-33, y=0, collidable=true, scale={x=-1, y=1}}
        }
    },
    desktopSide =   {
        itemCount   =   2,
        lenght      =   40,
        itemList    =   {
            {name='desktopSide', x=0, y=0, collidable=true},
            {name='deskChairSide', x=16, y=0, collidable=true}
        }
    },
    desktop     =   {
        itemCount   =   2,
        lenght      =   16,
        itemList    =   {
            {name='desktop', x=0, y=0, collidable=true},
            {name='deskChair', x=0, y=0, collidable=false}
        }
    },
    desktopAlt  =   {
        itemCount   =   2,
        lenght      =   16,
        itemList    =   {
            {name='desktop', x=0, y=0, collidable=false},
            {name='deskChair', x=0, y=0, collidable=true}
        }
    },
    pillarBig   =   {
        itemCount   =   5,
        lenght      =   16,
        itemList    =   {
            {name='pillarBot', x=0, y=0, collidable=true,bound=true},
            {name='pillarMid', x=0, y=32, collidable=true,bound=true},
            {name='pillarMid', x=0, y=64, collidable=true,bound=true},
            {name='pillarMid', x=0, y=96, collidable=true,bound=true},
            {name='pillarTop', x=0, y=128, collidable=true, bound=true}
        }
    },
    pillar      =   {
        itemCount   =   3,
        lenght      =   16,
        itemList    =   {
            {name='pillarBot', x=0, y=0, collidable=true,bound=true},
            {name='pillarMid', x=0, y=32, collidable=true,bound=true},
            {name='pillarTop', x=0, y=64, collidable=true, bound=true}
        }
    },
    pillarWide  =   {
        itemCount   =   5,
        lenght      =   16,
        itemList    =   {
            {name='pillarBot', x=-32, y=0, collidable=true,bound=true},
            {name='pillarMid', x=-32, y=32, collidable=true,bound=true},
            {name='pillarBot', x=32, y=0, collidable=true,bound=true},
            {name='pillarMid', x=32, y=32, collidable=true,bound=true},
            {name='pillarHardTop', x=0, y=64, collidable=true, bound=true}
        }
    },
    pillarWider =   {
        itemCount   =   5,
        lenght      =   16,
        itemList    =   {
            {name='pillarBot', x=-64, y=0, collidable=true,bound=true},
            {name='pillarMid', x=-64, y=32, collidable=true,bound=true},
            {name='pillarBot', x=64, y=0, collidable=true,bound=true},
            {name='pillarMid', x=64, y=32, collidable=true,bound=true},
            {name='pillarHardTopWide', x=0, y=64, collidable=true, bound=true}
        }
    },
    list    =   {'meetingTable', 'desktop', 'desktopSide', 'desktopAlt', 'pillar', 'pillarBig', 'pillarWide', 'pillarWider' }

}

--Creates a decoration set composed of several elements
--@param pX, pY coordinates of the beginning of the set in the platform
--@param pPlatform plateform to put the set on
--@param pSetName name of the set to use
function makeDecorationSet( pX, pY, pPlatform, pSetName )
    log( string.format( 'makeDecorationset( %d, %d, %s, %s )', pX, pY, tostring( pPlatform ), tostring( pSetName ) ) )
    local setName   =   pSetName or 'meetingTable'
    local blueprint =   nil
    if not DECORATION_SETS[setName] then
        blueprint   =   DECORATION_SETS['meetingTable']
    else
        blueprint   =   DECORATION_SETS[setName]
    end

    local currentSet    =   {}
    for i=1, blueprint.itemCount do
        local curItem   =   blueprint.itemList[i]
        local x =   pX + curItem.x
        local y =   pY + curItem.y
        if curItem.collidable then
            newItem         =   makeDecoration( x + pPlatform.topleft.x , y + pPlatform.topleft.y, curItem.name, curItem.collidable )
            newItem.body:setLinearVelocity( -chickProp.speed, 0 )
        else
            newItem     =   makeDecoration( x, (pPlatform.height*32)+16, curItem.name, curItem.collidable )
            newItem.prop2D:setParent( pPlatform.body )
            table.insert( pPlatform.decorations, newItem.prop2D )
        end

        if curItem.scale then
            newItem.prop2D:setScl( curItem.scale.x, curItem.scale.y )
        end
        table.insert( currentSet, newItem )
    end

    --Loop to bind the bodies  together when needed
    for i=200, #currentSet do --2 because we always link to the previous, so the first won't be linked anyway
        print('Shall I bind item #'..i)
        --table_print(currentSet[i])
        if currentSet[i].bound then
            print('Yes I shall')
            --table_print(curItem)
            local pivx, pivy      =   currentSet[i].prop2D:getPiv()
            local pivx2, pivy2    =   currentSet[i-1].prop2D:getPiv()
            local joint = world:addDistanceJoint( currentSet[i].body, 
                                                    currentSet[i-1].body, 
                                                    currentSet[i].x + pivx/2,
                                                    currentSet[i].y + pivy/2,
                                                    currentSet[i-1].x + pivx2/2,
                                                    currentSet[i-1].y + pivy2/2
                                                    )

            --Insert the joint in both table, so it can be destroid from anywhere
            table.insert( currentSet[i-1].joints, joint )
            table.insert( currentSet[i].joints, joint )
        end
    end
end

--Makes a ground platform (Similar to platforms except they have a padding to the bottom)
function makeGround(pX, pY, pLenght, pType)
    local x =   pX or 0
    local y =   pY or 0
    local lenght    =   pLenght or 20
    local height    =   y + (Stage.h/2)
    height  =   math.abs( height )
    height  =   height / 32 --In tiles
    height  =   math.floor( height )
    height  =   height + 1
    y   =   y - ( height * 32 )
    lenght  =   clamp( lenght, 10, 120 )
    pType   =   pType or 'full'
    if not GROUND_TYPE[pType] then
        pType   =   'full'
    end
    log( string.format("call to : makeGround(%f, %f, %d, %s)", pX, pY, lenght, pType) )

    local map   =   MOAIGrid.new()
    map:initRectGrid( lenght, height, 32, 32 )

    --Making the top Row ( wich comes last, oddly )
    local topRow    =   {}
    topRow[1]   =   height
    topRow[2]   =   GROUND_TYPE[pType]['topfirst']
    for i=3, lenght - 2 + 3 do
        topRow[i]   =   GROUND_TYPE[pType]['topmid']
    end
    topRow[#topRow] =   GROUND_TYPE[pType]['toplast']

    --Making the padding rows
    local padRow    =   {}
    padRow[1]   =   0
    padRow[2]   =   GROUND_TYPE[pType]['first']
    for i=3, lenght - 2 + 3 do
        padRow[i]   =   GROUND_TYPE[pType]['mid']
    end 
    padRow[#padRow] =   GROUND_TYPE[pType]['last']

    --Affecting padding rows to the map
    for i=1, height-1 do
        padRow[1]   =   i
        map:setRow( unpack( padRow ) )
    end
    --Finally the top row
    map:setRow( unpack( topRow ) )

    local ground    =   {
        prop2D  =   MOAIProp2D.new(),
        lenght  =   lenght,
        height  =   height,
        topleft =   {x=x, y=(height*32 + y)},
        decorations =   {},
        x       =   x,
        y       =   y
    }
    ground.prop2D:setDeck( platformSpriteSheet )
    ground.prop2D:setGrid( map )
    ground.prop2D:setLoc( 0, 0 )
    ground.prop2D:setColor( 1, 1, 1, 0 )
    ground.prop2D:seekColor( 1, 1, 1, 1, 0.5 )
    ground.body     =   world:addBody( MOAIBox2DBody.KINEMATIC, x, y )
    ground.body.tag =   'platform'

    ground.fixtures =   ground.body:addRect( 0, 0, 32*lenght, 32*height )
    ground.fixtures:setFriction( 0.9 )
    ground.fixtures:setRestitution( 0.0 )

    table.insert( GROUND_POOL, ground )
    ground.prop2D:setParent( ground.body )
    layer:insertProp( ground.prop2D )
    return ground
end

--Makes a platform at coords pX/pY of pLenght lenght of tiles
--@param pX, pY --> Coordinates to begin the platformm
--@param pLenght lenght of the platform in tiles
--@param pType type of platform (see PLATFORM_TYPE)
--@param pGround a ground platform to link with( optionnal )
function makePlatform( pX, pY, pLenght, pType, pGround )
    x       =   pX or 0
    y       =   pY or 0
    lenght  =   pLenght or 1
    lenght  =   clamp(lenght, 1, 10)
    pType   =   pType or "largeBeam"
    if not PLATFORM_TYPE[pType] then
        pType   =   'largeBeam'
    end
    log( string.format("call to : makePlatform(%f, %f, %d, %s)", pX, pY, lenght, pType) )

    local map       =   MOAIGrid.new()
    map:initRectGrid(lenght, 1, 32, 32)

    if lenght == 1 then --Single tile
        map:setRow(1, PLATFORM_TYPE[pType]['single'])
    else
        local tileList   =   {}
        tileList[1]=   1 --Row number 1
        tileList[2]=   PLATFORM_TYPE[pType]['first']--First tile
        for i=3, lenght - 2 + 3 do --Padding tiles...
            tileList[i]    =   PLATFORM_TYPE[pType]['mid']
        end
        tileList[#tileList]   =   PLATFORM_TYPE[pType]['last'] --Closing tile
        map:setRow( unpack(tileList) )
    end


    local height    =   PLATFORM_TYPE[pType]['height']
    local platform  =   {
        prop2D  =   MOAIProp2D.new(),
        lenght  =   lenght,
        height  =   height,
        topleft =   {x=x, y=(height*32 + y)},
        decorations =   {},
        pillars =   {},
        x       =   pX,
        y       =   pY,
    }
    platform.prop2D:setDeck( platformSpriteSheet )
    platform.prop2D:setLoc(0, 32*(height-1))
    platform.prop2D:setGrid(map)
    platform.prop2D:setColor( 1, 1, 1, 0 )
    platform.prop2D:seekColor( 1, 1, 1, 1, 0.5 )

    platform.body           =   world:addBody(MOAIBox2DBody.KINEMATIC, x, y)
    platform.body.tag       =   'platform'

    platform.fixtures  =   platform.body:addRect(0, 0, 32*lenght, 32*height) 
    platform.fixtures:setFriction(0.9)
    platform.fixtures:setRestitution(0.0)


    --Managing the base of the platform
    if pGround then
        --TODO: delta with ground and pillar it
    else
        local h =   (pY - Stage.h/2)/32
        h   =   math.abs(h)
        h   =   math.floor(h)
        h   =   h - 1 
        local firstPillar   =   makePillar( 'standard', h )

        firstPillar.prop2D:setLoc( 0, -h*32 )
        firstPillar.prop2D:addLoc( 0, 32-platform.height*32 )
        firstPillar.prop2D:setParent( platform.prop2D )
        table.insert( platform.pillars, firstPillar )


        if lenght > 1 then
            local secondPillar  =   makePillar( 'standard', h )
            secondPillar.prop2D:setLoc( (lenght-1)*32, -h*32 )
            secondPillar.prop2D:addLoc( 0, 32-platform.height*32 )
            secondPillar.prop2D:setParent( platform.prop2D )
            table.insert( platform.pillars, secondPillar )
        end
    end
    

    table.insert( PLATFORM_POOL, platform )
    platform.prop2D:setParent(platform.body)
    layer:insertProp( platform.prop2D )
    return platform
end

--Create a non-collidable pillar to sustain a platform
--@param pType model of pillar (see PILLAR_TYPE)
--@param pHeight height of the pillar in tiles
function makePillar( pType, pHeight )
    log( string.format( 'makePillar( %s, %d)', pType, pHeight ) )
    pType   =   pType or 'standard'
    pHeight =   pHeight or 10
    pHeight =   clamp( pHeight, 2, 25 )
    local pillar    =   {
        prop2D  =   MOAIProp2D.new(),
        height  =   pHeight,
        model   =   pType
    }
    local tiles =   PILLAR_TYPE[pType]
    local myMap =   MOAIGrid.new()
    myMap:initRectGrid( 1, pHeight, 32, 32 )

    myMap:setRow( 1, tiles.bot )
    for i=2, pHeight-1 do
        myMap:setRow( i, tiles.mid )
    end
    myMap:setRow( pHeight, tiles.top )

    pillar.prop2D:setDeck( platformSpriteSheet )
    pillar.prop2D:setGrid( myMap )
    pillar.prop2D:seekColor(1, 1, 1, 0.5, 0.5)
    backgroundLayer:insertProp( pillar.prop2D )

    return pillar
end

--Creates a decoration
--@param pCollidable sets the decoration an obstacle to block the player
function makeDecoration(pX, pY, pAsset, pCollidable)
    local x       =   pX or 0
    local y       =   pY or 0
    local pAsset  =   pAsset or 'chair'
    --print( string.format("call to : makeDecoration(%f, %f, %s)", x, y, pAsset) )


    pAsset      =   DECORATION_ARRAY[pAsset]
    local map   =   MOAIGrid.new()
    map:initRectGrid(#pAsset.map[1], #pAsset.map, 32, 32) --Lenght is lenght of the first row, height is size of the table
    for i=1, #pAsset.map do
        map:setRow( unpack( pAsset.map[i] ) )
    end
    
    local decoration=   {
        prop2D  =   MOAIProp2D.new(),
        x       =   x,
        y       =   y,
        joints  =   {},
        dead    =   false
    }
    decoration.prop2D:setDeck(platformSpriteSheet)
    decoration.prop2D:setGrid(map)
    decoration.prop2D:setColor(1, 1, 1, 0)
    if pCollidable then
        layer:insertProp( decoration.prop2D )
        --Setting up Prop2D
        decoration.prop2D:seekColor( 1, 1, 1, 1, 0.3 )
        decoration.prop2D:setPiv( pAsset.prop.piv.x, pAsset.prop.piv.y )

        --Setting up the body
        decoration.body           =   world:addBody( MOAIBox2DBody.DYNAMIC, x, y )
        decoration.body.tag       =   'obstacle'
        decoration.body:setMassData( pAsset.body.mass )
        decoration.body:setLinearVelocity( -chickProp.speed, 0)

        --print('body should be ok')
        --Function to get the root of decoration from the body part (useful to destroy it on collision)
        function decoration.body:getProp()
            return self.prop2D
        end

        function decoration.body:getDecoration()
            return self
        end

        function decoration:getJoint()
            return self.joint
        end

        function decoration:disJoint()
            for i=#self.joints, -1, 1 do
                self.joints[i]:destroy()
                self.joints[i]  =   nil
            end
        end
        
        --Setting up fixtures
        decoration.fixtures  =   decoration.body:addRect(
                                                    pAsset.fixture.size.xMin, 
                                                    pAsset.fixture.size.yMin, 
                                                    pAsset.fixture.size.xMax, 
                                                    pAsset.fixture.size.yMax 
                                                )

        decoration.fixtures:setRestitution( pAsset.fixture.restitution )
        decoration.fixtures:setFriction( pAsset.fixture.friction )
        decoration.fixtures:setDensity( pAsset.fixture.density )

        --Applying params and binding Prop2D to physical object
        decoration.body:resetMassData()
        decoration.prop2D:setParent(decoration.body)
        decoration.body.prop2D  =   decoration.prop2D
        table.insert( OBSTACLE_POOL, decoration )
        return decoration
    else
        backgroundLayer:insertProp( decoration.prop2D )
        decoration.prop2D:seekColor(1, 1, 1, 0.5, 0.5)
        decoration.prop2D:setLoc(x, y)
        decoration.prop2D:setPiv(16, 16)
        return decoration
    end
end

--Create a grount platform
--pX position of the last ground platform
--pY position of the last ground platform
--pRoom : does it have a room to go throught?
--pPlatform : does it have a platform as well?
function createNextGroundPlatform( pX, pY, pRoom, pPlatform )
    local nType =   math.random( 1, tableSize( GROUND_TYPE.list ) )
    nType   =   GROUND_TYPE.list[nType]
    local xPos  =   math.random( pX, pX + 64 + chickProp.speed )
    local yPos  =   math.random( pY - 32, pY + 32 )
    yPos    =   clamp( yPos, (-Stage.h/2) + 50, (-Stage.h/2 + 80) )
    local len   =   math.random( 10, 70 )
    --log( string.format( 'call to : createNextGroundPlatform(%f, %f, %b)', pX, pY, pRoom ) )
    local currentGround = makeGround( xPos, yPos, len, nType )
    if pRoom then
        --TODO: For now we add two sets of decoration : make actual rooms later pls
        local nDecorationSet    =   math.random( 1, tableSize(DECORATION_SETS.list) )
        nDecorationSet  =   DECORATION_SETS.list[nDecorationSet]
        makeDecorationSet( len * 32 / 2, 0, currentGround, nDecorationSet )

        local newDecorationSet    =   math.random( 1, tableSize(DECORATION_SETS.list) )
        newDecorationSet  =   DECORATION_SETS.list[newDecorationSet]
        local setLen    =   DECORATION_SETS[newDecorationSet].lenght
        makeDecorationSet( (len * 32)-setLen*2, 0, currentGround, newDecorationSet )
    end

    if pPlatform then
        --TODO:Create platform
        local nType         =   math.random( 1, tableSize(PLATFORM_TYPE.list) )
        nType   =   PLATFORM_TYPE.list[nType]
        local y =   math.random( 1, 3 )
        y   =   y * 32
        local lenght        =   math.random( 1, 35 )
        local platform  =   makePlatform( pX, pY + y, lenght, nType )
        --TODO:Create platform with range of the ground
        --TODO:Create platform out of range of any decoration (h and w)
    end
end

--Create a platform for the next hop
--pX position of the last platform
--pY position of the last platform
--pDecoration if it must have some decorations
--pObstacles if it must have some obstacles
function createNextPlatform( pX, pY, pDecoration, pObstacles )
    --log( string.format("call to : createNextPlatform(%f, %f, %b, %b)", pX, pY, pDecoration, pObstacles) )
    local nPlatforms    =   math.random( 1, 1 ) --Number of platforms to create

    for i=1, nPlatforms do
        local nType         =   math.random( 1, tableSize(PLATFORM_TYPE.list) )
        nType   =   PLATFORM_TYPE.list[nType]
        local lenght        =   math.random( 1, 35 )
        local nDecorations  =   math.random( 0, lenght/2 )
        local nObstacles    =   math.random( 0, lenght/2 )
        local xPosition     =   math.random( pX + 32, pX + 128 + chickProp.speed ) 
        local yPosition     =   math.random( pY - 32, pY + 32 )
        local currentPlatform   =   makePlatform( xPosition, yPosition, lenght, nType )
        local nDecorationSet    =   math.random( 1, tableSize(DECORATION_SETS.list) )
        nDecorationSet  =   DECORATION_SETS.list[nDecorationSet]
        makeDecorationSet( lenght*16/2, 0, currentPlatform, nDecorationSet )
    end
end


-- Destroys a platform and removes it from the pool
function destroyPlatform( pPlatform )
    log('Destroying a platform')
    --Destroying decorations
    if pPlatform.decorations then
        for j, v in pairs( pPlatform.decorations ) do
            backgroundLayer:removeProp( v )
        end
    end

    --Destroying pillars
    if pPlatform.pillars then
        for i, v in pairs( pPlatform.pillars ) do
            destroyPillar( v )
        end
    end
    layer:removeProp( pPlatform.prop2D )
    pPlatform.body:destroy()
    for i=1, #PLATFORM_POOL do
        if PLATFORM_POOL[i] == pPlatform then
            table.remove( PLATFORM_POOL, i )
            log( 'Destroying Platform. Remaining : '..#PLATFORM_POOL )
            return
        end
    end
end

function destroyGround( pGround )
    if pGround.decorations then
        for j, v in pairs( pGround.decorations ) do
            backgroundLayer:removeProp( v )
        end
    end
    layer:removeProp( pGround.prop2D )
    pGround.body:destroy()
    for i=1, #GROUND_POOL do
        if GROUND_POOL[i] == pGround then
            table.remove( GROUND_POOL, i )
            log( 'Destroying Ground. Remaining : '..#GROUND_POOL )
            return
        end
    end
end

function destroyObstacleByBody( pBody )
    log('Remaining obstacles: '..#OBSTACLE_POOL)
    layer:removeProp( pBody:getDecoration().prop2D )
    pBody:destroy()
    for i=1, #OBSTACLE_POOL do
        if OBSTACLE_POOL[i].body == pBody then
            log( 'Destroying Obstacle. Remaining : '..#OBSTACLE_POOL )
            table.remove( OBSTACLE_POOL, i )
            return
        end
    end
end

function destroyObstacle( pObstacle )
    layer:removeProp( pObstacle.prop2D )
    pObstacle.dead  =   true
    pObstacle.body:destroy()
    for i=1, #OBSTACLE_POOL do
        if OBSTACLE_POOL[i] == pObstacle then
            table.remove( OBSTACLE_POOL, i )
            log( 'Destroying Obstacle. Remaining : '..#OBSTACLE_POOL )
            return
        end
    end
end

function destroyPillar( pPillar )
    backgroundLayer:removeProp( pPillar.prop2D )
    log( 'Destroying pillar.' )
end
