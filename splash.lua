if DEBUG then
    MOAIDebugLines.setStyle ( MOAIDebugLines.TEXT_BOX, 1, 1, 1, 1, 1 )
    MOAIDebugLines.setStyle ( MOAIDebugLines.TEXT_BOX_LAYOUT, 1, 0, 0, 1, 1 )
    MOAIDebugLines.setStyle ( MOAIDebugLines.TEXT_BOX_BASELINES, 1, 1, 0, 0, 1 )
end

--========================================
--Setting up layers specifics to splash screen
--========================================
splashHudLayer    =   MOAILayer2D.new()
splashHudLayer:setViewport( viewport )

splashLayer	=	MOAILayer2D.new()
splashLayer:setViewport(viewport)
splashLayer:setCamera( camera )

splashBackgroundLayer =   MOAILayer2D.new()
splashBackgroundLayer:setViewport( viewport )
splashBackgroundLayer:setCamera( camera )

splashLayerTable  =   {
    splashBackgroundLayer,
    splashLayer,
    splashHudLayer
}

--Declaring elements of the logo
local yellowCircle  =   MOAIGfxQuad2D.new()
yellowCircle:setTexture( "images/splashscreen/yellow.png" )
yellowCircle:setRect( -256, -256, 256, 256 )
local yellowCircleProp  =   MOAIProp2D.new()
yellowCircleProp:setDeck( yellowCircle )
splashBackgroundLayer:insertProp( yellowCircleProp )

local redOutline    =   MOAIGfxQuad2D.new()
redOutline:setTexture( "images/splashscreen/red.png" )
redOutline:setRect( -256, -256, 256, 256 )
local redOutlineProp    =   MOAIProp2D.new()
redOutlineProp:setDeck( redOutline )
redOutlineProp:setColor( 0, 0, 0, 0 )
splashHudLayer:insertProp( redOutlineProp )

local blackTitleBar =   MOAIGfxQuad2D.new()
blackTitleBar:setTexture( "images/splashscreen/black.png" )
blackTitleBar:setRect( -256, -256, 256, 256 )
local blackTitleBarProp =   MOAIProp2D.new()
blackTitleBarProp:setDeck( blackTitleBar )
blackTitleBarProp:setColor( 1, 1, 1, 0 )
splashHudLayer:insertProp( blackTitleBarProp )

local titleTextBox  =   MOAITextBox.new()
titleTextBox:setStyle( whiteTitleStyle )
titleTextBox:setString( "Jurassic Run" )--TODO: Replace with game name variable/constant
titleTextBox:setRect( -Stage.w/2, -Stage.h/4, Stage.w/2, Stage.h/4 )
titleTextBox:setAlignment ( MOAITextBox.CENTER_JUSTIFY, MOAITextBox.CENTER_JUSTIFY )
titleTextBox:setLoc( 0, -90 )
titleTextBox:setYFlip( true )
titleTextBox:setScl( 0.5, 0.5 )
titleTextBox:setVisible( true )
titleTextBox:setColor( 1, 1, 1, 0 )
splashHudLayer:insertProp( titleTextBox )

local messageTextBox    =   MOAITextBox.new()
messageTextBox:setStyle( stdWhiteStyle )
messageTextBox:setRect( -Stage.w/2, -Stage.h/4, Stage.w/2, Stage.h/4 )
messageTextBox:setLoc( 0, -200 )
messageTextBox:setAlignment ( MOAITextBox.CENTER_JUSTIFY, MOAITextBox.CENTER_JUSTIFY )
messageTextBox:setYFlip( true )
messageTextBox:setString( "Tap to continue" )
messageTextBox:setColor( 1, 1, 1, 0 )
messageTextBox:setScl( 0.6, 0.6 )
splashHudLayer:insertProp( messageTextBox )



local spine = require "spine.spine"
local splashTrex =   {}
--Setting up spine animations and skeleton
--========================================
local loader = spine.AttachmentLoader:new()
function loader:createImage(attachment)

  local deck = MOAIGfxQuad2D.new()
  deck:setTexture("./animations/trex/" .. attachment.name .. ".png")
  deck:setUVRect(0, 0, 1, 1)
  deck:setRect(0, 0, attachment.width, attachment.height)
  
  local prop = MOAIProp.new()
  prop:setDeck(deck)  
  prop:setPiv(attachment.width / 2, attachment.height / 2)
  splashLayer:insertProp(prop)

  return prop
end


local json = spine.SkeletonJson:new(loader)
json.scale = 0.8

local skeletonData = json:readSkeletonDataFile("./animations/trex/skeleton.json")

splashTrex.skeleton = spine.Skeleton:new(skeletonData)
splashTrex.skeleton.prop:setParent(splashTrex.body)
--splashTrex.skeleton.prop:setAttrLink( MOAIProp2D.INHERIT_TRANSFORM, splashTrex.body, MOAIProp2D.TRANSFORM_TRAIT ) 
splashTrex.skeleton.flipX = false
splashTrex.skeleton.flipY = true
if DEBUG then
    splashTrex.skeleton.debugBones = true
end
splashTrex.skeleton.debugLayer = splashLayer
splashTrex.skeleton:setToBindPose()

splashTrex.animationStateData = spine.AnimationStateData:new(skeletonData)
splashTrex.skeleton.prop:setLoc( -200, 0 )

splashTrex.animationState = spine.AnimationState:new(splashTrex.animationStateData)
splashTrex.animationState:setAnimation( "intro", false )

--Displays the logo with a nice tweening effect
function displayLogo()
    blackTitleBarProp:seekColor( 1, 1, 1, 1, 1.5 )
    redOutlineProp:seekColor( 1, 1, 1, 1, 1.5 )
    titleTextBox:seekColor( 1, 1, 1, 1, 5.5 )

    --titleTextBox:seekScl( 0.58, 0.55, 2.5 )
    messageTextBox:seekColor( 1, 1, 1, 1, 6.5 )
end

--Animation thread fot the rex kid !
MOAIThread.new():run(function()
  currentTime =   MOAISim.getDeviceTime()
  while true do
    --Running behavior
    local dt    =   MOAISim.getStep() 
    splashTrex.animationState:apply(splashTrex.skeleton)
    splashTrex.skeleton:updateWorldTransform()
    
    --TODO: EPIC HACK CORRECT THIS ASAP THX!

    local   previousTime    =   currentTime
    currentTime =   MOAISim.getDeviceTime()
    DELTATIME   =   currentTime - previousTime
    
    splashTrex.animationState:update( MOAISim.getStep() )
    if splashTrex.animationState.currentTime > splashTrex.animationState.current.duration then
        displayLogo()
    end
    coroutine.yield()
  end
end)
