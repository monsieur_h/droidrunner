--==========================
--File handling the controls for both mouse and touch inputs
--==========================

function handlePointer(x, y)
    --chickProp:setLoc(layer:wndToWorld(x,y))
    print("Shall jump now")
end

function clickHandler( pBool )
    if GAMESTATE == 'gameover' then
        gameStateHandler( 'play' )
    elseif GAMESTATE == 'splashscreen' then
        gameStateHandler( 'play' )
    else
        chickProp:jump( pBool )
    end
end

function onMouseLeftEvent(mousedown)
        clickHandler( mousedown )
end

function onTouchEvent(eventType, idx, x, y, tapCount)
    if(eventType == MOAITouchSensor.TOUCH_DOWN) then
        clickHandler( true )
    elseif(eventType == MOAITouchSensor.TOUCH_UP) then
        clickHandler( false )
    end
end

if MOAIInputMgr.device.pointer then
    print("Found a MOUSE pointer, setting callbacks...")
    MOAIInputMgr.device.mouseLeft:setCallback(onMouseLeftEvent)
else
    print("Found a TOUCH device, setting callbacks...")
    MOAIInputMgr.device.touch:setCallback(onTouchEvent)
end
