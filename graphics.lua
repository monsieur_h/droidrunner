print( 'Loading graphics...' )
print("Starting MOAI on : " ..MOAIEnvironment.osBrand)
if MOAIEnvironment.osVersion then
    print("OS version : "..MOAIEnvironment.osVersion)
end
if MOAIEnvironment.screenWidth then
    print("Detected resolution : "..MOAIEnvironment.screenWidth..'x'..MOAIEnvironment.screenHeight)
end

if AUDIO then
    print("Starting sound engine...")
    MOAIUntzSystem.initialize()
    MOAIUntzSystem.setVolume( 1 )
end

Screen  =   {
    w   =   MOAIEnvironment.screenWidth or 1280,
    h   =   MOAIEnvironment.screenHeight or 768
}

Stage   =   {
    w   =   1280/1.5,
    h   =   768/1.5
}

MOAISim.openWindow( "droidRunner", Screen.w, Screen.w )

--Camera settings
camera  =   MOAICamera2D.new()
camera.maxZoom  =   1.3
camera.minZoom  =   1.0
camera.mode     =   'none'
camera.modes    =   {
    close   =   {x=0, y=0, scl=0.9, duration=3},
    far     =   {x=150, y=5, scl=1.0, duration=3},
    closeup =   {x=0, y=0, scl=0.5, duration=0.5}
}

function camera:setMode( pMode )
    if self.modes[pMode]  and pMode ~= self.mode then
        log( 'Setting camera mode to : '..pMode )
        local thread    =   MOAIThread.new()
        local m =   self.modes[pMode]
        local x, y  =   camera:getLoc()
        local sclx, scly    =   camera:getScl()
        thread:run( easeCamera, m.x-x, m.y-y, 0, m.scl-sclx, m.scl-scly, m.duration )
    end
    self.mode   =   pMode
end

function easeCamera( ... )
    wait( camera:move( ... ) )
end

viewport	=	MOAIViewport.new()
viewport:setSize(Screen.w, Screen.h)
viewport:setScale(Stage.w, Stage.h)

--========================================
--Setting up layers
--========================================
hudLayer    =   MOAILayer2D.new()
hudLayer:setViewport( viewport )

layer	=	MOAILayer2D.new()
layer:setViewport(viewport)
layer:setCamera( camera )

backgroundLayer =   MOAILayer2D.new()
backgroundLayer:setViewport( viewport )
backgroundLayer:setCamera( camera )

layerTable  =   {
    backgroundLayer,
    layer,
    hudLayer
}
MOAIRenderMgr.setRenderTable( layerTable )

function updateClearColor( pColor )
    print('updating color')
    print( pColor )
    table_print( pColor )
    if MOAIGfxDevice.getFrameBuffer then --This to conserve compatibility
        MOAIGfxDevice.getFrameBuffer():setClearColor( unpack( pColor ) )
    elseif MOAIGfxDevice.setClearColor then
        MOAIGfxDevice.setClearColor( unpack( pColor ) )
    end
end

--========================================
--World configuration
--========================================
--Preparing the Deck holding the tilesheet
platformSpriteSheet =   MOAITileDeck2D.new()
platformSpriteSheet:setTexture("images/minimal_industry_spritesheet.png")
platformSpriteSheet:setSize(29, 40)

--Tilts the camera
--@param pDegree maximum degrees moved during the shake
--@param pDuration duration of the tilt in seconds
function cameraTilt( pX, pY, pDegree, pDuration )
    pX  =   pX or 5
    pY  =   pY or 5
    pDegree =   pDegree or 0.6
    pDuration   =   pDegree or 0.3
    wait( camera:move(  pX/3,    pY/3,       pDegree/3,      0,  0,  pDuration/3 ) )
    wait( camera:move( -pX/1.5, -pY/1.5,    -pDegree/1.5,    0,  0,  pDuration/3 ) )
    wait( camera:move(  pX/3,    pY/3,       pDegree/3,      0,  0,  pDuration/3 ) )
end

