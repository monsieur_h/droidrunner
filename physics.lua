print( 'Starting physics simulation...' )

--========================================
--Instanciate Physics World
--========================================
world   =   MOAIBox2DWorld.new()
world:setGravity(0, -45)
world:setUnitsToMeters(1/15)
world:start()

if DEBUG then
    layer:setBox2DWorld(world)
    world:setDebugDrawFlags( MOAIBox2DWorld.DEBUG_DRAW_SHAPES + MOAIBox2DWorld.DEBUG_DRAW_JOINTS +
                             MOAIBox2DWorld.DEBUG_DRAW_PAIRS  + MOAIBox2DWorld.DEBUG_DRAW_CENTERS )
    MOAIDebugLines.setStyle ( MOAIDebugLines.TEXT_BOX, 1, 1, 1, 1, 1 )
    MOAIDebugLines.setStyle ( MOAIDebugLines.TEXT_BOX_LAYOUT, 1, 0, 0, 1, 1 )
    MOAIDebugLines.setStyle ( MOAIDebugLines.TEXT_BOX_BASELINES, 1, 1, 0, 0, 1 )
end

--Makes all the decorations tilt when the player lands on a platform
--@param pStrenght specifies the strengh of the bump
--@param pPhysics wether it should be applied or, just use the camera shake
function bumpDecoration( pStrenght, pPhysics )
    if pPhysics then
        pStrenght   =   pStrenght * 50000
        log( string.format( 'Bumping the area with a total strengh of %f', pStrenght ) )
        for i, obstacle in pairs( OBSTACLE_POOL ) do
            if obstacle.body then
                local cx, cy    =   chickProp.body:getPosition()
                local ox, oy    =   obstacle.body:getPosition()
                if ox and oy then
                    local dist      =   distance2(cx, cy, ox, oy)
                    obstacle.body:applyLinearImpulse( 0, pStrenght/dist)
                end
            end
        end
    end
    local thread    =   MOAIThread.new()
    thread:run( cameraTilt )
end

