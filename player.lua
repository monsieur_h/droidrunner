print( 'Loading spine runtime...' )
local spine = require "spine.spine"
print( 'Loading player...' )

--========================================
--Player configuration
--========================================
chickImg    =   MOAIGfxQuad2D.new()
chickImg:setTexture("images/chick.png")
chickImg:setRect(-32, -32, 32, 32)

--Instanciating the chicken 
chickProp   =   {}
chickProp.speed         =   120
chickProp.acceleration  =   80
chickProp.impactMalu    =   1.0 --Multiplicated by speed on each collision
chickProp.maxSpeed      =   350
chickProp.minSpeed      =   50
chickProp.contactCount  =   0
chickProp.stoped        =   true
chickProp.score         =   0
chickProp.runDuration   =   0

--The body of the player itself:
chickProp.body  =   world:addBody(MOAIBox2DBody.DYNAMIC)
chickProp.body:isBullet( true )
chickProp.body.tag  =   'player'
chickProp.body:setFixedRotation( true )
chickProp.fixtures =   {
    hitBox      =   chickProp.body:addCircle( 0, 0, 20 ),
    frontSensor =   chickProp.body:addRect( 15, -12, 22, 2 )      --Bumping rect
}
chickProp.fixtures.hitBox:setRestitution( 0 )
chickProp.fixtures.hitBox:setFriction( 0 )
chickProp.fixtures.hitBox:setDensity( 0.023 )
chickProp.fixtures.frontSensor:setDensity( 0.003 )
chickProp.fixtures.frontSensor:setSensor( true )

chickProp.body:resetMassData()


--The attach to limit the body to one DOF:
chickProp.attach    =   world:addBody(MOAIBox2DBody.STATIC, 0, Stage.h)
chickProp.attach.fixtures   =   chickProp.attach:addRect(-1, -1, 1, 1)
local ax, ay    =   chickProp.body:getWorldCenter()
local bx, by    =   chickProp.attach:getWorldCenter()
joint   =   world:addPrismaticJoint(chickProp.body, chickProp.attach, ax, ay, bx, by, 0, 1)
joint:setLimit( -Stage.h/2, Stage.h )


--Loading sounds
--========================================
chickProp.sounds    =   {
    roar        =   MOAIUntzSound.new(),
    roarlong    =   MOAIUntzSound.new(),
    playing     =   false
}
chickProp.sounds.roar:load( 'sounds/roar-short.wav' )
chickProp.sounds.roarlong:load( 'sounds/roar-long.wav' )

--Setting up spine animations and skeleton
--========================================
local loader = spine.AttachmentLoader:new()
function loader:createImage(attachment)

  local deck = MOAIGfxQuad2D.new()
  deck:setTexture("./animations/trex/" .. attachment.name .. ".png")
  deck:setUVRect(0, 0, 1, 1)
  deck:setRect(0, 0, attachment.width, attachment.height)
  
  local prop = MOAIProp.new()
  prop:setDeck(deck)  
  prop:setPiv(attachment.width / 2, attachment.height / 2)
  layer:insertProp(prop)

  return prop
end


local json = spine.SkeletonJson:new(loader)
json.scale = 0.2

local skeletonData = json:readSkeletonDataFile("./animations/trex/skeleton.json")

chickProp.skeleton = spine.Skeleton:new(skeletonData)
chickProp.skeleton.prop:setParent(chickProp.body)
chickProp.skeleton.prop:setAttrLink( MOAIProp2D.INHERIT_TRANSFORM, chickProp.body, MOAIProp2D.TRANSFORM_TRAIT ) 
chickProp.skeleton.flipX = false
chickProp.skeleton.flipY = true
if DEBUG then
    chickProp.skeleton.debugBones = true
end
chickProp.skeleton.debugLayer = layer
chickProp.skeleton:setToBindPose()

chickProp.animationStateData = spine.AnimationStateData:new(skeletonData)
chickProp.animationStateData:setMix("run", "jump", 0.4)
chickProp.animationStateData:setMix("jump", "fall", 0.3)
chickProp.animationStateData:setMix("fall", "land", 0.25)
chickProp.animationStateData:setMix("land", "run", 0.2)
chickProp.animationStateData:setMix("roar", "run", 0.2)

chickProp.animationStateData:setMix("run", "bump", 0.18)
chickProp.animationStateData:setMix("bump", "run", 0.18)

 
chickProp.animationState = spine.AnimationState:new(chickProp.animationStateData)
chickProp.animationState:setAnimation("roar", false)
--========================================
require 'controls'

function chickProp:accelerate( pDelta )
    chickProp.runDuration   =   chickProp.runDuration + pDelta
    chickProp.score         =   chickProp.score + ( pDelta * chickProp.speed )
    if self.stoped or chickProp.animationState.current.name == 'roar' then
        self.speed  =   0
    else
        self.speed  =   self.speed + (self.acceleration * pDelta)
        self.speed  =   clamp( self.speed, self.minSpeed, self.maxSpeed )
        scoreTextbox:setString( string.format( 'Speed : %d\nScore : %d', self.speed, self.score ) )
        if math.floor(self.speed) ~= lastSpeed then --Update the HUD
            if self.speed > 2*self.maxSpeed/3 then
                camera:setMode( 'far' )
            else
                camera:setMode( 'close' )
            end
        end
        lastSpeed   =   math.floor(self.speed)
    end
end

function chickProp:jump(upward)
    if upward then
        log('Jumping with contact count'..chickProp.contactCount)
        if self.contactCount > 0 then
            self.body:applyLinearImpulse(0,8200)
        end
    end
end

function chickProp:hitObstacle( pFixture )
    self.speed  =   self.speed * self.impactMalu
    self.obstacle   =   pFixture:getBody()
    --pFixture:getBody():applyLinearImpulse( chickProp.speed, 0 )
    chickProp.animationState:setAnimation( 'bump', false )
end

function chickProp:hitPlatform()
    chickProp.speed =   0
    chickProp.stoped=   true
end

-- player front sensor
function frontSensorHandler( phase, fix_a, fix_b, arbiter )
    if phase == MOAIBox2DArbiter.BEGIN then
        if fix_b:getBody().tag == 'obstacle' then
            chickProp:hitObstacle( fix_b )
        elseif fix_b:getBody().tag == 'platform' then
            chickProp:hitPlatform()
        end
    end
    --if phase == MOAIBox2DArbiter.BEGIN then
        --chickProp.contactCount  =   chickProp.contactCount + 1
    --elseif phase == MOAIBox2DArbiter.END then
        --chickProp.contactCount  =   chickProp.contactCount - 1
    --end
end
chickProp.fixtures.frontSensor:setCollisionHandler( frontSensorHandler, MOAIBox2DArbiter.BEGIN + MOAIBox2DArbiter.END )

-- player foot sensor
function footSensorHandler( phase, fix_a, fix_b, arbiter )
    if phase == MOAIBox2DArbiter.BEGIN then 
        if fix_b:getBody().tag == 'platform' then
            local x, speedY =   fix_a:getBody():getLinearVelocity()
            local xa, ya    =   fix_a:getBody():getPosition()
            local xb, yb    =   fix_b:getBody():getPosition()
            local dist      =   distance2( xa, ya, xb, yb )
            bumpDecoration( 10*chickProp.body:getMass(), true )
            chickProp.contactCount  =   chickProp.contactCount + 1
            chickProp.stoped    =   false
        elseif fix_b:getBody().tag == 'obstacle' then
            chickProp.contactCount  =   chickProp.contactCount + 1
            --chickProp.stoped    =   false
        end
    elseif phase == MOAIBox2DArbiter.END then
        chickProp.contactCount  =   chickProp.contactCount - 1
    end
end
chickProp.fixtures.hitBox:setCollisionHandler( footSensorHandler, MOAIBox2DArbiter.BEGIN + MOAIBox2DArbiter.END )

-- Setting up a sensor to destroy falling object
bottomScreenSensor  =   world:addBody( MOAIBox2DBody.STATIC )
bottomScreenSensor.fixture =   bottomScreenSensor:addRect( -Stage.w/2 , -Stage.h/2 - 50, Stage.w/2, -Stage.h/2 - 40 ) --TODO:Tweak with non-empiric values
bottomScreenSensor.fixture:setSensor( true )
