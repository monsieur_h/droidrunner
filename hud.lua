print( 'Starting HUD' )
--========================================
--Populating HUD
--========================================
charcodes = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 .,:;!?()&/-'

dejavuFont    =   MOAIFont.new()
dejavuFont:load( 'fonts/DejaVuSans-ExtraLight.ttf' )
dejavuFont:preloadGlyphs( charcodes, 24 )

jurrasicFont    =   MOAIFont.new()
jurrasicFont:load( 'fonts/jurassic.ttf' )
jurrasicFont:preloadGlyphs( charcodes, 24 )

whiteTitleStyle =   MOAITextStyle.new()
whiteTitleStyle:setFont( jurrasicFont )
whiteTitleStyle:setSize( 250 )
whiteTitleStyle:setColor( 1, 1, 1, 1 )


stdWhiteStyle        =   MOAITextStyle.new()
stdWhiteStyle:setFont( dejavuFont )
stdWhiteStyle:setSize( 24 )
stdWhiteStyle:setColor( 1, 1, 1, 1 )

stdStyle        =   MOAITextStyle.new()
stdStyle:setFont( dejavuFont )
stdStyle:setSize( 24 )
stdStyle:setColor( 0, 0, 0, 1 )

titleStyle        =   MOAITextStyle.new()
titleStyle:setFont( dejavuFont )
titleStyle:setSize( 152 )
titleStyle:setColor( 0, 0, 0, 1 )

--Displays the speed and the time
scoreTextbox    =   MOAITextBox.new()
scoreTextbox:setScl( 0.5, 0.5 )
scoreTextbox:setRect( -Stage.w, -Stage.h, Stage.w, Stage.h )
scoreTextbox:setStyle( stdStyle )
scoreTextbox:setString("Hello World")
scoreTextbox:setYFlip( true )
hudLayer:insertProp( scoreTextbox )

--Displays the 'Game Over' message
gameOverTextBox =   MOAITextBox.new()
gameOverTextBox:setScl( 0.5, 0.5 )
gameOverTextBox:setRect( -Stage.w/1.5, -Stage.h/4, Stage.w/1.5, Stage.h/4 )
gameOverTextBox:setStyle( titleStyle )
gameOverTextBox:setAlignment ( MOAITextBox.CENTER_JUSTIFY, MOAITextBox.CENTER_JUSTIFY )
gameOverTextBox:setString( "GAME OVER" )
gameOverTextBox:setYFlip( true )
gameOverTextBox:setVisible( false )
hudLayer:insertProp( gameOverTextBox )
